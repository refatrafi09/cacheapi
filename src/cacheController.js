import express from 'express';
const router = express.Router();
import bodyParser from 'body-parser';
import faker from 'faker';
import _ from 'underscore';
router.use(bodyParser.urlencoded({ extended: true }));
import Cache from './cache';

let fetchedCache = [];

// function for checing cache hit or cache miss 
function isKeyThere(keyName) {
    return new Promise(resolve => {
        Cache.find({}, function (err, caches) {
            if (err) return res.status(500).send("There was a problem finding the cache.");
            fetchedCache = caches;
            resolve(_.findLastIndex(caches, {key: keyName}) !== -1 ? false : true );
        });
    });
}

// API for cache hit or cache miss 
router.post('/addCache', function (req, res) {
    const { key } = req.body;
    isKeyThere(key).then((notFound) => {
        if (notFound === true) {
            Cache.create({
                key : req.body.key,
                value : faker.random.word(),
                ttl: new Date(),
            }, 
            function (err, cache) {
                if (err) return res.status(500).send("There was a problem adding the information to the database.");
                console.log('Cache miss');
                res.status(200).send(cache);
            });
        } else {
            console.log('Cache hit');
            Cache.findOne({key}, function(err, cache) {
                if(!err) {
                    cache.ttl = new Date(); // Updating the time to live
                    cache.save(function(err) {
                        if(!err) {
                            res.status(200).send(cache);
                        }
                        else {
                            console.log("Error: could not save Cache");
                        }
                    });
                }
            });
        }
    })
});

// API for updating a particular key in the cache
router.post('/updateCache', function (req, res) {
    const { key } = req.body;
    Cache.findOne({key}, function(err, cache) {
        if(!err && cache) {
            cache.ttl = new Date();
            cache.value = faker.random.word();
            cache.save(function(err) {
                if(!err) {
                    res.status(200).send(cache);
                }
                else {
                    res.status(200).send({error: 'Error: could not save Cache'});
                    console.log("Error: could not save Cache");
                }
            });
        } else {
            console.log('Error: could not Update Cache');
            res.status(200).send({error: 'Error: could not Update Cache. Please add cache First'});
        }
    });
});

// API for removing a particular key in the cache
router.post('/removeCache', function (req, res) {
    const { key } = req.body;
    Cache.findOne({key}, function(err, cache) {
        if(!err && cache) {
            cache.remove();
            res.status(200).send({'success': 'Cache is delete'});
        } else {
            console.log('Error: could not Delete Cache');
            res.status(200).send({error: 'Error: could not Update Cache. Please add cache First'});
        }
    });
});

// API for updating all key in the cache
router.post('/removeallCache', function (req, res) {
    Cache.remove({}, function(err, row) {
        if (err) {
            console.log("Collection couldn't be removed" + err);
            res.send("Collection couldn't be removed" + err);
        }
        res.status(200).send({'success': 'Entire Cache is delete'});
    })
});

// API for fetching all key in the cache
router.get('/', function (req, res) {
    Cache.find({}, function (err, caches) {
        if (err) return res.status(500).send("There was a problem finding the Caches.");
        res.status(200).send(caches);
    });
});
export default router;