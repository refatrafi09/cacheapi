import mongoose from 'mongoose';  
const CacheSchema = new mongoose.Schema({  
  key: String,
  value: String,
	ttl: {type: Date, expires: 600}, // Auto Deletes in 60 seconds
});
mongoose.model('Cache', CacheSchema);
export default mongoose.model('Cache');