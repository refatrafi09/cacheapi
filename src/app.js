import express from 'express';
import CacheController from'./cacheController';
import db from './db.js'; //ADD THIS LINE

const app = express();

app.use('/caches', CacheController);

export default app;