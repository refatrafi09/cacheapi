'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _faker = require('faker');

var _faker2 = _interopRequireDefault(_faker);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _cache = require('./cache');

var _cache2 = _interopRequireDefault(_cache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.use(_bodyParser2.default.urlencoded({ extended: true }));


var fetchedCache = [];

function isKeyThere(keyName) {
    return new Promise(function (resolve) {
        _cache2.default.find({}, function (err, caches) {
            if (err) return res.status(500).send("There was a problem finding the cache.");
            fetchedCache = caches;
            resolve(_underscore2.default.findLastIndex(caches, { key: keyName }) !== -1 ? false : true);
        });
    });
}

router.post('/', function (req, res) {
    isKeyThere(req.body.key).then(function (isthere) {
        _cache2.default.create({
            key: req.body.key,
            value: _faker2.default.random.word(),
            ttl: new Date()
        }, function (err, cache) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(cache);
        });
    });
});

router.post('/addCache', function (req, res) {
    var key = req.body.key;

    isKeyThere(key).then(function (notFound) {
        if (notFound === true) {
            _cache2.default.create({
                key: req.body.key,
                value: _faker2.default.random.word(),
                ttl: new Date()
            }, function (err, cache) {
                if (err) return res.status(500).send("There was a problem adding the information to the database.");
                console.log('Cache miss');
                res.status(200).send(cache);
            });
        } else {
            console.log('Cache hit');
            _cache2.default.findOne({ key: key }, function (err, cache) {
                if (!err) {
                    cache.ttl = new Date(); // Updating the time to live
                    cache.save(function (err) {
                        if (!err) {
                            res.status(200).send(cache);
                        } else {
                            console.log("Error: could not save Cache");
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateCache', function (req, res) {
    var key = req.body.key;

    _cache2.default.findOne({ key: key }, function (err, cache) {
        if (!err && cache) {
            cache.ttl = new Date();
            cache.value = _faker2.default.random.word();
            cache.save(function (err) {
                if (!err) {
                    res.status(200).send(cache);
                } else {
                    res.status(200).send({ error: 'Error: could not save Cache' });
                    console.log("Error: could not save Cache");
                }
            });
        } else {
            console.log('Error: could not Update Cache');
            res.status(200).send({ error: 'Error: could not Update Cache. Please add cache First' });
        }
    });
});

router.post('/removeCache', function (req, res) {
    var key = req.body.key;

    _cache2.default.findOne({ key: key }, function (err, cache) {
        if (!err && cache) {
            console.log(cache);
            cache.remove();
            res.status(200).send({ 'success': 'Cache is delete' });
        } else {
            console.log('Error: could not Delete Cache');
            res.status(200).send({ error: 'Error: could not Update Cache. Please add cache First' });
        }
    });
});

/*
ModelName.remove({}, function(err, row) {
  if (err) {
      console.log("Collection couldn't be removed" + err);
      return;
  }

  console.log("collection removed");
})
*/

router.post('/removeallCache', function (req, res) {
    _cache2.default.remove({}, function (err, row) {
        if (err) {
            console.log("Collection couldn't be removed" + err);
            res.send("Collection couldn't be removed" + err);
        }
        res.status(200).send({ 'success': 'Entire Cache is delete' });
    });
});

router.get('/', function (req, res) {
    _cache2.default.find({}, function (err, caches) {
        if (err) return res.status(500).send("There was a problem finding the Caches.");
        res.status(200).send(caches);
    });
});
exports.default = router;