'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CacheSchema = new _mongoose2.default.Schema({
  key: String,
  value: String,
  ttl: { type: Date, expires: 600 } // Auto Deletes in 60 seconds
});
_mongoose2.default.model('Cache', CacheSchema);
exports.default = _mongoose2.default.model('Cache');