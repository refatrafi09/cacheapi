'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cacheController = require('./cacheController');

var _cacheController2 = _interopRequireDefault(_cacheController);

var _db = require('./db.js');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//ADD THIS LINE

var app = (0, _express2.default)();

app.use('/caches', _cacheController2.default);

exports.default = app;