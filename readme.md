After cloning this repo execute these following comamnds from terminal

```
yarn
yarn start
```

So, the server is up on port 3000 of local node server.

Using postman or curl test the following api.

```
Fetching all keys of the cache api: {GET} http://localhost:3000/caches/
Checking Cache miss or Cache Hit(pass key a key in the body): {POST} http://localhost:3000/caches/addCache
Updating a single Key(pass key a key in the body): {POST} http://localhost:3000/caches/updateCache
Deleting a particular key(pass key a key in the body): {POST} http://localhost:3000/caches/removeCache
Deleting entire cache keys: {POST} http://localhost:3000/caches/removeallCache
```

